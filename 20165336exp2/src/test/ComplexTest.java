import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;
public class ComplexTest extends TestCase {
    Complex c1 = new Complex(0, 3);
    Complex c2 = new Complex(-1, -1);
    Complex c3 = new Complex(2,1);
    Complex c4 = new Complex(1,1);
    Complex c5 = new Complex(3,2);
    Complex c6 = new Complex(0,-2);
    @Test
    public void testgetRealPart() throws Exception {
        assertEquals(-1.0, new Complex().setRealPart(-1.0));
        assertEquals(5.0, new Complex().setRealPart(5.0));
        assertEquals(0.0, new Complex().setRealPart(0.0));
    }
    @Test
    public void testgetRealPart1() throws Exception {
        assertEquals(1.0, new Complex().setRealPart(1.0));
        assertEquals(4.0, new Complex().setRealPart(4.0));
        assertEquals(3.0, new Complex().setRealPart(3.0));
    }
    @Test
    public void testgetImagePart() throws Exception {
        assertEquals(-1.0, new Complex().setImagePart(-1.0));
        assertEquals(5.0, new Complex().setImagePart(5.0));
        assertEquals(0.0, new Complex().setImagePart(0.0));
    }
    @Test
    public void testgetImagePart1() throws Exception {
        assertEquals(1.0, new Complex().setImagePart(1.0));
        assertEquals(4.0, new Complex().setImagePart(4.0));
        assertEquals(3.0, new Complex().setImagePart(3.0));
    }
    @Test
    public void testComplexAdd() throws Exception {
        assertEquals("-1.0+2.0i", c1.ComplexAdd(c2).toString());
        assertEquals("2.0+4.0i", c1.ComplexAdd(c3).toString());
        assertEquals("1.0", c2.ComplexAdd(c3).toString());
    }
    @Test
    public void testComplexAdd1() throws Exception {
        assertEquals("4.0+3.0i", c4.ComplexAdd(c5).toString());
        assertEquals("1.0-1.0i", c4.ComplexAdd(c6).toString());
        assertEquals("3.0", c5.ComplexAdd(c6).toString());
    }
    @Test
    public void testComplexSub() throws Exception {
        assertEquals("-1.0-4.0i", c1.ComplexMinus(c2).toString());
        assertEquals("2.0-2.0i", c1.ComplexMinus(c3).toString());
        assertEquals("3.0+2.0i", c2.ComplexMinus(c3).toString());
    }
    @Test
    public void testComplexMulti() throws Exception {
        assertEquals("3.0-3.0i", c1.ComplexMulti(c2).toString());
        assertEquals("-3.0+6.0i", c1.ComplexMulti(c3).toString());
        assertEquals("-1.0-3.0i", c2.ComplexMulti(c3).toString());
    }
    @Test
    public void testComplexDiv() throws Exception {
        assertEquals("-1.5-1.5i", c1.ComplexDiv(c2).toString());
        assertEquals("0.6+1.2i", c1.ComplexDiv(c3).toString());
        assertEquals("-0.6-0.2i", c2.ComplexDiv(c3).toString());
    }
}