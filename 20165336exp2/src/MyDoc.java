abstract class Data {
    abstract public void DisplayValue();
}
class Byte extends  Data {
    byte value;
    Byte() {
        value=127;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
abstract class Factory {
    abstract public Data CreateDataObject();
}
class ByteFactory extends Factory {
    public Data CreateDataObject(){
        return new Byte();
    }
}
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
public class MyDoc {
    static Document d;
    public static void main(String[] args) {
        d = new Document(new ByteFactory());
        d.DisplayData();
    }
}