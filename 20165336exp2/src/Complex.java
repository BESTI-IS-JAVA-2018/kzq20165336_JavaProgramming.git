public class Complex {
    double a,b;
    public Complex(){
        this.a = 0;
        this.b = 0;
    }
    public Complex(double a, double b) {
        this.a = a;
        this.b = b;
    }
    public  double getRealPart(){
        return this.a;
    }
    public double getImagePart(){
        return this.b;
    }
    public double setRealPart(double a){
        this.a = a;
        return a;
    }
    public double setImagePart(double b){
        this.b = b;
        return b;
    }
    Complex ComplexAdd(Complex c){
        double a = c.getRealPart();
        double b = c.getImagePart();
        double newA = a + this.a;
        double newB = b + this.b;
        Complex Result = new Complex(newA,newB);
        return Result;
    }
    Complex ComplexMinus(Complex c){
        double a = c.getRealPart();
        double b = c.getImagePart();
        double newA = a - this.a;
        double newB = b - this.b;
        Complex Result = new Complex(newA,newB);
        return Result;
    }
    Complex ComplexMulti(Complex c){
        double a = c.getRealPart();
        double b = c.getImagePart();
        double newA = this.a*a-this.b*b;
        double newB = this.b*a+this.a*b;
        Complex Result = new Complex(newA,newB);
        return Result;
    }
    Complex ComplexDiv(Complex c){
        double a = c.getRealPart();
        double b = c.getImagePart();
        double newA = (this.a*a+this.b*b)/(a*a+b*b) ;
        double newB = ((this.b*a-this.a*b)/(a*a+b*b));
        Complex Result = new Complex(newA,newB);
        return Result;
    }
    public String toString() {
        String s = " ";
        if (b > 0)
            s =  a + "+" + b + "i";
        if (b == 0)
            s =  a + "";
        if (b < 0)
            s = a + "" + b + "i";
        return s;
    }
}
