# 20165336 2017-2018-2 《Java程序设计》第1周学习总结

## 教材学习内容总结

- Java地位、特点：Java具有面向对象、与平台无关、安全、稳定和多线程等优良特性。Java的平台无关性让Java成为编写网络应用程序的佼佼者。
- JDK安装：安装中需要注意细节，根据教程顺利安装。
- JAVA简单程序编写，保存，编译，运行：注意编码时代码的输入，需要牢记代码的内容。
- Java反编译

## 教材学习中的问题和解决过程

- （解决）问题1：设置环境变量中的系统变量时，path值时%Java_Home%\bin前系统默认会加入符号‘，使得JAVAC无法运行
解决方案：删除符号‘
- （未解决）问题2：windows10中安装好bash后无法下载tree尝试多种网络解决方法未果。
- （未解决）问题3：windows10 bash中用vim编辑器无法编辑中文汉字。

### [我的码云链接https://gitee.com/BESTI-IS-JAVA-2018/kzq20165336_JavaProgramming.git](https://gitee.com/BESTI-IS-JAVA-2018/kzq20165336_JavaProgramming.git)

![](https://images2018.cnblogs.com/blog/1296581/201803/1296581-20180304195148398-1808816338.png)


 （statistics.sh脚本的运行结果截图）
![](https://images2018.cnblogs.com/blog/1296581/201803/1296581-20180304195200061-233622346.png)



![](https://images2018.cnblogs.com/blog/1296581/201803/1296581-20180304195207250-1745547867.png)

![](https://images2018.cnblogs.com/blog/1296581/201803/1296581-20180304195213561-22923205.png)
