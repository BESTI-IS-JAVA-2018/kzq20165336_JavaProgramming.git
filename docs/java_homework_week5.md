# 20165336 2017-2018-2 《Java程序设计》第5周学习总结

## 教材学习内容总结

1. 内部类的类体中不可以声明类变量和类方法。
2. 内部类仅供他的外嵌类使用，其他类不可以用某个类的内部类声明对象。
3. 内部类可以被修饰为static内部类。
4. static内部类不能操作外嵌类中的实例成员变量。
5. 非内部类不可以是static类。
6. 匿名类可以访问外嵌类中的成员变量和方法，匿名类的类体中不可以声明static成员变量和方法。
7. 由于匿名类是一个子类，但没有类名，所以在用匿名类创建对象时，要直接使用父类的构造方法。
8. 异常类：Java使用throw关键字抛出一个Exception子类的实例表示异常发生。
9. 在调试程序是可以使用-ea启用断言语句。
10. throw和throws是两个不同的关键字。
11. 创建一个File对象的构造方法：File（String filename）；、File（String directoryPath，String filename）；、File（File dir，String filename）；
12. File对象调用方法public boolean mkdir（）创建一个目录，如果创建成功返回true，否则返回false。
13. public String[] list（） 用字符串形式返回目录下的全部文件。
14. public File[] listFiles()用File对象形式返回目录下的全部文件。
15. 使用输入流的步骤：1.设定输入流的源，2.创建指向源的输入流，3.让输入流读取源中的数据，4.关闭输入流。
16. 文件字节输入、输出流的read和write方法使用字节数组读写数据，即以字节为单位处理数据。
17. 文件字符输入、输出流：FileReader, FileWriter，缓冲流：BufferedReader, BufferedWriter，随机流 RandomAccessFile。

## 代码调试中的问题和解决过程
- 问题1：IDEA中文乱码
- 问题1解决方案：将UTF-8改为GBK即可。

## [代码托管](https://gitee.com/BESTI-IS-JAVA-2018/kzq20165336_JavaProgramming/tree/master/src)

（statistics.sh脚本的运行结果截图）
![](https://images2018.cnblogs.com/blog/1296581/201804/1296581-20180401194932068-1528045257.png)
![](https://images2018.cnblogs.com/blog/1296581/201804/1296581-20180401194943931-682988112.png)