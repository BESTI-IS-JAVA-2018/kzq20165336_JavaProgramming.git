import java.util.*;

public class MyDC {
    private final char ADD = '+';
    private final char SUBTRACT = '-';
    private final char MUTIPLY = '*';
    private final char DIVIDE = '/';
    private Stack<Integer> stack;
    public MyDC(){
        stack = new Stack<Integer>();
    }
    public int evaluate(String expr){
        int op1,op2,result = 0;
        String token;
        StringTokenizer tokenizer = new StringTokenizer(expr);
        while(tokenizer.hasMoreTokens()){
            token = tokenizer.nextToken();
            if(isOperator(token)){
                op2 = (stack.pop().intValue());
                op1 = (stack.pop().intValue());
                result = evalSingleOp(token.charAt(0),op1,op2);
                stack.push(new Integer(result));
            }
            else {
                stack.push(new Integer((Integer.parseInt(token))));
            }
        }
        return result;
    }
    private boolean isOperator(String token){
        return (token.equals("+")||token.equals("-")||token.equals("*")||token.equals("/"));
    }
    private int evalSingleOp(char operation,int op1,int op2) {
        int result = 0;
        switch (operation){
            case ADD:
                result = op1+op2;
                break;
            case SUBTRACT:
                result = op1-op2;
                break;
            case MUTIPLY:
                result = op1*op2;
                break;
            case DIVIDE:
                result = op1/op2;
        }
        return result;
    }
}