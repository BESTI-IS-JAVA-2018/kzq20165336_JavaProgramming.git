public class Test{
	public static void main(String args[]){
	CPU cpu= new CPU();
	cpu.setSpeed(2200);
	HardDisk disk= new HardDisk();
	disk.setAmount(200);
	PC pc = new PC();
	pc.setCPU(cpu);
	pc.setHardDisk(disk);
	pc.show();
	System.out.println(cpu.toString());
	System.out.println(pc.toString());
	System.out.println(disk.toString());
	System.out.println("是否重写CPU中equals方法："+cpu.equals());
	System.out.println("是否重写PC中equals方法："+pc.equals());
	System.out.println("是否重写HardDisk中equals方法："+disk.equals());
	}
}